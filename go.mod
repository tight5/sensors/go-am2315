module gitlab.com/tight5/sensors/go-am2315

require (
	github.com/d2r2/go-i2c v0.0.0-20181113114621-14f8dd4e89ce
	github.com/d2r2/go-logger v0.0.0-20181221090742-9998a510495e // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pkg/errors v0.8.1
)

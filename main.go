package am2315

import (
	"time"

	"github.com/d2r2/go-i2c"
	"github.com/pkg/errors"
)

const (
	R_CONST         = 8314.3
	MW_CONST        = 18.016
	AM2315_I2CADDR  = 0x5c
	AM2315_WAITTIME = 1500
	MAXTRYS         = 3
	AM2315_READ     = 0x03
)

var (
	i2cClient *i2c.I2C
)

type Closer interface {
	Close() error
}

// InitI2C connects to I2C bus 2 (rasp 2&3) and returns a client for the caller
// to Close.
func InitI2C() (Closer, error) {
	var err error
	i2cClient, err = i2c.NewI2C(AM2315_I2CADDR, 1)
	if err != nil {
		return nil, errors.Wrap(err, "failed to connect to i2c bus")
	}
	return i2cClient, nil
}

// Read returns temperature & humidity respectively from the sensor.
func Read() (float64, float64, error) {
	var (
		tmp []byte
	)
	for i := 0; i < MAXTRYS; i++ {
		i2cClient.WriteRegU8(AM2315_READ, 0x00)
		time.Sleep(time.Nanosecond * AM2315_WAITTIME)
		i2cClient.WriteBytes([]byte{AM2315_READ, 0x00, 0x04})
		time.Sleep(time.Nanosecond * AM2315_WAITTIME)
		tmp = make([]byte, 8)
		_, err := i2cClient.ReadBytes(tmp)
		if err != nil {
			return 0, 0, err
		}

		if tmp[2] != 0 || tmp[3] != 0 ||
			tmp[4] != 0 || tmp[5] != 0 {
			break
		}
	}
	if tmp[2] != 0 || tmp[3] != 0 ||
		tmp[4] != 0 || tmp[5] != 0 {
		return 0, 0, errors.New("failed to read am2315")
	}

	humidity := ((float64(tmp[2]) * 256) + float64(tmp[3])) / 10.0
	temperature := ((float64(uint16(tmp[4]&0x7F)) * 256) + float64(tmp[5])) / 10.0
	if tmp[4]&0x80 != 0 {
		temperature = -temperature
	}
	crc := (256 * uint16(tmp[7])) + uint16(tmp[6])
	expectedCRC := calcCRC(tmp[:6])
	if crc != expectedCRC {
		return 0, 0, errors.New("crc error in sensor data")
	}
	return temperature, humidity, nil
}

// checkCRC Returns the 16-bit CRC of sensor data
func calcCRC(pts []byte) uint16 {
	crc := 0xFFFF
	for _, l := range pts {
		crc = crc ^ int(l)
		for i := 1; i < 9; i++ {
			if crc&0x01 != 0 {
				crc = crc >> 1
				crc = crc ^ 0xA001
			} else {
				crc = crc >> 1
			}
		}
	}
	return uint16(crc)
}

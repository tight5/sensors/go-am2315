AM2315
==

Go Library for reading data off the [AM2315](https://cdn-shop.adafruit.com/datasheets/AM2315.pdf)

## Usage

```
import (
	"gitlab.com/tight5/go-am2315"
	"github.com/adamryman/kit/logger"
)

func main() {
	i2c, err := am2315.InitI2C()
	if err != nil {
		logger.LogError(err)
		os.Exit(1)
	}
	defer i2c.Close()

	t, h, err := am2315.Read()
	if err != nil {
		logger.LogError(err)
		break FOR
	}
	logger.Info().Log("sensor", "am2315", "temp", t, "humidity", h)
}
```
